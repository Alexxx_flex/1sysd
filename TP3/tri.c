#include <stdio.h>

int main() {
    // Définir un tableau constant d'une douzaine de valeurs
    const int tableauConstant[12] = {42, 12, 15, 10, 5, 9, 2, 47, 88, 12, 66, 54};

    // Afficher le contenu du tableau
    printf("Contenu du tableau constant : ");
    for (int i = 0; i < 12; ++i) {
        printf("%d", tableauConstant[i]);

        // Ajouter un espace sauf pour le dernier nombre
        if (i < 11) {
            printf(" ");
        }
    }

    // Autres actions avec le tableau constant...

    return 0;
}
