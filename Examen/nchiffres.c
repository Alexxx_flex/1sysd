#include <stdio.h>
#include <string.h>

int n_chiffres(char *s) {
    int count = 0;
    for (int i = 0; i < strlen(s); i++) 
    {
        if (s[i] >= '0' && s[i] <= '9') {
            count++;
        }
    }
    
    return count;
}

int main(int argc, char *string[]) {
    if (argc != 2) {
        printf("Il faut écire la fonction comme ça: ./nchiffres [string] \n") ;
        return 1;
    }

    printf("Il y a %d chiffre(s) dans votre réponse\n", n_chiffres(string[1]));
}

