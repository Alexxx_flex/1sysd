#include <stdio.h>

int nocase_equal(char *s1, char *s2) {
    while (*s1 && *s2) {
        if ((*s1 != *s2) && (*s1 != *s2 + 32) && (*s1 != *s2 - 32)) {
            return 0;
        }
        s1++;
        s2++;
    }
    if (*s1 == *s2) {
        return 1;
    }
    else {
        return 0;
    }
}

int main(int argc, char *string[]) 
{
    if (argc != 3) {
        printf("Il faut écrire la fonction comme ca: ./nocase_cmp [string1] [string2]\n", string[0]);
        return 1;
    }

    if (nocase_equal(string[1], string[2])) {
        printf("Identiques\n");
    } else {
        printf("Différentes\n");
    }

}

