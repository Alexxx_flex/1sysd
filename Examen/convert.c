#include<stdio.h>
#include<stdlib.h>

float inches2cm(float val) {
    return val * 2.54;
}

float cm2inches(float val) {
    return val / 2.54;
}

int main() {
    char ans;
    float val;
    
    ans = ' '; // valeur initiale invalide
    while (ans != '1' && ans != '2') {
        printf("(1) cm vers inches ou (2) inches vers cm ? ");
        scanf(" %c", &ans);
    }
    switch (ans) {
        case '1':
            printf("Valeur en cm : ");
            scanf("%f", &val);
            printf("En inches : %.2f\n", cm2inches(val));
            break;
        case '2':
            printf("Valeur en inches : ");
            scanf("%f", &val);
            printf("En cm : %.2f\n", inches2cm(val));
            break;
    }
    exit(0);
}
