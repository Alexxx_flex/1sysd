#include<stdio.h>
#include<time.h>
#include<stdlib.h>

int main() {
    int secret, guess;

    srand(time(NULL)); 
    secret = rand() % 10 + 1;

    printf("J'ai tiré un nombre au hasard entre 1 et 10\n");
    printf("Devinez le : ");
    scanf("%d", &guess);

    if (secret == guess) {
        printf("Trouvé !\n");
    } else {
        printf("Perdu ! C'était %d\n", secret);
    }
}


