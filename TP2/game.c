#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    // Initialiser le générateur de nombres aléatoires avec une graine basée sur le temps
    srand(time(NULL));

    // Générer un nombre aléatoire entre 1 et 100
    int nombreSecret = rand() % 100 + 1;

    int proposition, tentatives = 0;

    printf("Devinez le nombre entre 1 et 100.\n");

    do {
        printf("Proposition : ");
        scanf("%d", &proposition);

        // Incrémenter le nombre de tentatives
        tentatives++;

        if (proposition == nombreSecret) {
            printf("Félicitations, vous avez deviné le nombre en %d tentatives!\n", tentatives);
        } else if (proposition < nombreSecret) {
            printf("Le nombre est plus grand.\n");
        } else {
            printf("Le nombre est plus petit.\n");
        }

    } while (proposition != nombreSecret);

    return 0;
}
